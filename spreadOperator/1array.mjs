
// spread operator are wrapper opener
// spread operator are used to make new array from existing arrays

let ar1 = ["a", "b"]
let ar2 = ["x", "y", "z"]

let ar3 = [...ar2, 2, 3,...ar1]

console.log(ar3)