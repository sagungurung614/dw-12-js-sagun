console.log(true === true); //true
console.log(1 === 1); //true
console.log(false === false); //true
console.log(undefined === undefined); //true

console.log(NaN === NaN); //false
// even though NaN has the type number which is primitive it gives false result because "a"*"b" !== "c"*"d"
