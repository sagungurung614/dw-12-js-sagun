let a = NaN;

console.log(a === NaN); //false

// to check a variable is NaN or not use isNaN function

console.log(isNaN(a)); //true

// obj.func() ==> method
// func() ==>function
