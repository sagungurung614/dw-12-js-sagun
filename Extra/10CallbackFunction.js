let fun1 = () => {
  // .....
};

let fun2 = (_fun1) => {
  // .....
};

fun2(fun1);

// callback function => function which is passed
// fun2 is higher order function
// function which takes another function as an argument = higher order function
