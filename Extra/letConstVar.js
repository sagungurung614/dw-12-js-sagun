

// var has functional scope

var c=3

// let has block level scope

let a = 1;
 const b = 2

 a=20
//  b=283 
// we cannot reinitialize the const variable

