if (true) {
  let a = 1;

  if (true) {
    let b = 0;
    if (true) {
      console.log(a);
    }
  }
}

//p1
// a=1

// p2
// b=0

// p3

// scope chaining is the scope of variables in a block, if the variable isn't mentioned in a child or grandchild block
// then it search on its parents block and so on
