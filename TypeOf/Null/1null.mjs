
let a;
console.log(a) //undefined

a=null;
console.log(a) //null

// undefined means variable is defined but left untouched/or not used yet
// null means a variable has null value or a variable is defined but initialized to empty
// typeof(null) => object which is a bug in JS because only non-primitive type are objects
