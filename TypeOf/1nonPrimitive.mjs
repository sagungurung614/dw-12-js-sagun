
//  1) Array
let nameArr = ["Sagun", "Chauchau", "Momo", "Susu", "Amba"]

console.log(typeof(nameArr)) //Object

// 2) Object
let infoObject = {
    name: "Sagun",
    hungry: true,
    age: 100,
}

console.log(typeof(infoObject)) //object

