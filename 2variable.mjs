console.log("Let's learn variable")
// define variable
let name ="Sagun" //let creates the container in a memory space
let age = 24
let isMarried = false

console.log(name)
console.log(age)
console.log(isMarried)

name = "Nitan" //change the value of the variable
age = 30
console.log(name)
console.log(age)
console.log(isMarried)

// console.log(address)