
// let name = " My name  is Sagun   "

// console.log(name.length)
// console.log(name.toUpperCase())
// console.log(name.toLowerCase())

// console.log(name.trimStart())
// console.log(name.trimEnd())
// console.log(name.trim())

// console.log(name.startsWith("S"))
// console.log(name.endsWith("S"))

// console.log(name.replace("Sagun", "Susu"))
// console.log(name.replace("n", "m")) //replace method will replace only the first match

// console.log(name.replaceAll("n", "s"))// it will replace all the match

let string = "    My Name   "

let stringFunction = (str) => {
    return str.trim()
}

console.log(stringFunction(string))