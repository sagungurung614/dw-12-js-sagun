
// let value = Math.random() => (0,1) // it gives any value between 0 and 1 or (0,1)

// let value1 = Math.random() * 100 // it means any value bet (0, 100)

// console.log(value1)

// let ceilValue = Math.ceil(1.33) //it gives highest integer value of 1.33

// console.log(ceilValue)

// console.log(Math.floor(3.33)) // it gives lowest integer value of 3.33

// if we need value from 5 to 15 or (5, 15)
let number = Math.random() * 10 + 5 // (0,1)*10+5 => (0, 10)+5 => (5, 15)
console.log(number)