import {fruit} from '../../1index.mjs'
import abc from '../../8importExport.mjs' //import by default


// while importing by default we do not need curly braces and we can use any variable
console.log(fruit)
console.log(abc)
