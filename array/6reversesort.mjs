// There is no descending sort in JS hence we use reverse()

let ar1 = ["b", "z", "c"]

let ar2 = ar1.sort().reverse()
console.log(ar2)