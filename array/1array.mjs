//array is used to store data of same or different type
let names= ["Ram", "Shyam", "Hari", 30, true]

console.log(names) //retrieving all elements from array
console.log(names[0]) //retrieving particular element from array
console.log(names[1])
console.log(names[2])

// Changing element of an array

names[1] = "Sita"
console.log(names)