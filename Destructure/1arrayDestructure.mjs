
// let list = [1, 2, 3, 4]
// 
// array can also be defined as:
let[a, b,c,d] = [1, 2, 3, 4]
// here the order matters
console.log(a)
console.log(b)
console.log(c)
console.log(d)