
// let info = {name: "Sag", age: 100, isMarried: false}

// we can also defined object as:
// in object destructuring, order does not matter

let[name, age, isMarried] = {name:"Sag",age: 100,isMarried: true}

console.log(name)
console.log(age)
console.log(isMarried)