// Export by Name
export let address = "Budhanilkantha"

export let college = "ABCDGISUG"
export let school = "pqwohqpihsq"

// Export by Default
let food = "momo"
export default food
// export by default can only be done once per file while export by name can be done as many as we need

// to check alias in default
// let fruit = "Apple"
// export default fruit //alias in default doesn't work