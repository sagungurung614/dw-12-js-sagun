
/* 
 == and === difference

 == -> it gives true if it has same value
=== -> it gives true if both value and data type is same otherwise false

*/

console.log(1==1)
console.log(1=== 1)

console.log("1" == 1)
console.log("1" === 1)