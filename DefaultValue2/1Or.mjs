
// or can be used as default value

let name = "" || false || undefined || null || 0 || "Sagun"

console.log(name)

// if value is falsy, it checks next one
// if all are false, the output will be last value