
let name = false ?? undefined ?? 0 ?? "Sagun" ?? null

console.log(name)

//  falsy values are (null and undefined)
// if value is falsy, it checks next one
// if all are false, the output will be last value