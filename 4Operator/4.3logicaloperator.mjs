// &&, ||, !
//  && = true, if all are true else false
// || = true, if any one is true

//OR Operator
// console.log(true)
// console.log(true && true)
// console.log(true && false && true)

// AND Operator
// console.log(true)
// console.log(true || false)
// console.log(true || false || true)
// console.log(false || false|| false)

// Not Operator
console.log(!true) // result false
console.log(!false) // true