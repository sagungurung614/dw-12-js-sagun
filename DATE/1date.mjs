
let currentDate = new Date()

console.log(currentDate)

// invalid ISO format - 2024/2/21T6:3:33 => here 2 must be 02 and 6 must be 06, because it takes two number
// so the correct format is - 2024/02/21/T06:03:33
// 