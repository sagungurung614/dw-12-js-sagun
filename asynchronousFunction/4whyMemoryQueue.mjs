
setTimeout(() => {
    console.log("3 sec")
}, 3000)

setTimeout(()=>{
    console.log("0 second")
}, 0)

setTimeout(()=>{
    console.log("1 sec")
}, 1000)



/* 
    any task that takes 5 sec
    => 1
*/