
// Does setTimeOut() guarantee the exact time?
// it depends upon the other JS code/function we are running along with async function
// - Nope but it indicates the minimum execution time of that asynchronous function
// -setTimeOut() guarantee the min execution time but doesn't guarantee the exact time